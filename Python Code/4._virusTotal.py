# *****************************************************************************************************************************
# CIS-553
# Examining the Exploitation of Trending Topics by Cybercriminals on the Internet
# Group members: A. Dalal, J.Sharma, P.Srivastav
# virusTotal.py
# The Python script utilizes the VirusTotal API to scan and analyze a subset of URLs extracted from a CSV file, where each URL 
# corresponds to a trending topic obtained from sources like Google and Bing. The script includes three main blocks of functionality:
# scanning the URLs using the VirusTotal API, retrieving and storing scan reports, and generating a comprehensive output that
# combines the original CSV data with the obtained VirusTotal reports. To ensure compliance with VirusTotal API rate limits, 
# the script incorporates a sleep mechanism and the option to rotate through multiple API keys. The end result is a set of output files
# containing scan responses, detailed reports, and a combined CSV file with enriched information for further analysis.
# *****************************************************************************************************************************

from pathlib import Path
import json
import time
import csv
from collections import deque
from tqdm import tqdm
import mycredentials
from cryptography.fernet import Fernet

# Get the parent directory of the current file
BASE_DIRECTORY = Path(__file__).resolve().parent

try:
    import requests
except ImportError:
    raise ImportError("This script requires the requests package to work. Install it using 'pip install requests'")

# Function to get the total number of rows in a CSV file
def get_total_rows(csv_file_path):
    with open(csv_file_path, newline='') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        total_rows = sum(1 for row in csv_reader)
    return total_rows

# Function to scan URLs using VirusTotal API
def scan_urls(url_batch, api_key):
    url = 'https://www.virustotal.com/vtapi/v2/url/scan'
    scan_id_list = []
    for URL in url_batch:
        try:
            params = {'apikey': api_key, 'url': URL}
            response = requests.post(url, data=params)
            scan_id_list.append(response.json()['scan_id'])
        except ValueError as e:
            print(URL)
            print("Rate limit detected:", e, URL)
            scan_id_list.append("Scan Rate limit detected: " + URL)
            continue
        except Exception:
            print("Error detected:", URL)
            scan_id_list.append(" Scan Exception Occured at URL: " + URL)
            continue

    return scan_id_list

# Function to get the report for scanned URLs from VirusTotal API
def get_scan_reports(scan_id_list, api_key):
    url = 'https://www.virustotal.com/vtapi/v2/url/report'
    report_list = []

    for id in scan_id_list:
        try:
            params = {'apikey': api_key, 'resource': id}
            response = requests.get(url, params=params)
            report_list.append(response.json())
        except ValueError as e:
            print("Rate limit detected: ", e, id)
            report_list.append(" Reporting Rate limit detected: " + id)
            continue
        except Exception:
            print("Error detected: ", id)
            report_list.append(" Reporting Exception Occured at URL: " + id)
            continue

    return report_list

# Function to process a CSV file and extract URLs and corresponding input data
def process_csv_file(link_file, start_row, end_row, last_N_rows_only=False):
    url_list = []
    input_data_list = []

    if last_N_rows_only:
        with open(link_file, newline='') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            
            # Skip rows until the start_row
            for _ in range(start_row):
                next(csv_reader, None)
            
            # Read and store the specified number of rows
            for _ in range(end_row - start_row + 1):
                try:
                    row = next(csv_reader)
                    url_list.append(row[4])
                    input_data_list.append(",".join(row))
                except StopIteration:
                    # Handle the case where end_row is beyond the end of the file
                    break

    else:
        with open(link_file, newline='') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            
            for row in csv_reader:
                url_list.append(row[4])
                input_data_list.append(",".join(row))

    return url_list, input_data_list


def main():

    # Define file paths for input and output
    link_file = BASE_DIRECTORY / "_Output/Links.csv"
    response_fp = BASE_DIRECTORY / "_Output/vtresponsefile.txt"  # Output file for Code Block 1
    output_fp = BASE_DIRECTORY / "_Output/vt_output.txt"          # Output file for Code Block 2
    output_fp_csv = BASE_DIRECTORY / "_Output/vt_output.csv"      # Output file for Code Block 3
    cipher_suite = Fernet(mycredentials.encryption_key)
    vt_api_key = [
                    cipher_suite.decrypt(mycredentials.encrypted_vt_api_key).decode()
                    #.....Key 2
                    #.....Key 3
                 ]

    # Define VirusTotal API parameters
    sleep_time = 70       # Sleep for > 60 sec for API cooldown, else VT will throw limit reached error
    key_index = 0         # Index to rotate through multiple API keys (Unused for now)
    vt_item_cnt = 0       # Count to check how many items need to be processed
    vt_max_batch_cnt = 500  # Max number of URLs that can be processed in one day
    limit_per_min = 4      # Max URLs that can be processed in one minute

    start_index = mycredentials.trend_per_day * mycredentials.url_link_per_trend_per_day * 2 # multiplied by 2 because we are doing Google & Bing
    start_row = get_total_rows(link_file) - start_index
    end_row = get_total_rows(link_file)          

    code_block_1_vt_scan = True
    code_block_2_vt_report = True
    code_block_3_gen_output = True

    # last_N_rows_only - Set it False if you want to run the entire CSV
    url_list, input_list = process_csv_file(link_file, start_row, end_row, last_N_rows_only=True)

    # Code Block 1: Scan URLs using VirusTotal API
    response = []
    report_list = []
    if code_block_1_vt_scan:
        response_file = open(response_fp, 'a')
        response_file.truncate(0)  # Clear the file before using to avoid using stale data
        for i in tqdm(range(len(url_list))):
            if i % limit_per_min == 0:
                # API cooldown time is 60 seconds
                print ("API Cool down time. Wait for a minimum of 60 secs")
                time.sleep(sleep_time)
                url_batch = []
            url_batch.append(url_list[i])
            vt_item_cnt += 1
            # Break out of the for loop if trying to process more URLs than the allowed limit.
            if vt_item_cnt > vt_max_batch_cnt:
                print('Max batch count reached. Hence exit')
                break
            if i % limit_per_min == (limit_per_min - 1) or i == len(url_list) - 1:
                if vt_item_cnt % vt_max_batch_cnt == 0:
                    key_index += 1
                    if key_index >= len(vt_api_key):
                        key_index = 0
                # print ("key_index: ", key_index)
                response += scan_urls(url_batch, vt_api_key[key_index])
                response_file.write('\n'.join(str(t) for t in response))
                print('Scanned ', i+1, ' URLs')
        if len(url_list) != len(input_list):
            print("Warning: URL List and Input list mismatch detected")
        response_file.close()

    # Code Block 2: Get reports for scanned URLs from VirusTotal API
    if code_block_2_vt_report:
        output_file = open(output_fp, 'a')
        output_file.truncate(0)  # Clear the file before using to avoid using stale data

        print('\nReport in progress...\n')
        for i in tqdm(range(len(response))):
            if i % limit_per_min == 0:
                print ("API Cool down time. Wait for a minimum of 60 secs")
                time.sleep(sleep_time)
                scan_list = []
                print('Reported ', i, ' URLs')
            scan_list.append(response[i])
            vt_item_cnt += 1
            # Break out of the for loop if trying to process more URLs than the allowed limit.
            if vt_item_cnt > vt_max_batch_cnt:
                print('Max batch count reached. Hence exit')
                break
            if (i % limit_per_min == (limit_per_min - 1) or i == len(response) - 1):
                if vt_item_cnt % vt_max_batch_cnt == 0:
                    key_index += 1
                    if key_index >= len(vt_api_key):
                        key_index = 0
                report_batch = get_scan_reports(scan_list, vt_api_key[key_index])
                report_list += report_batch
                for r in report_batch:
                    json.dump(r, output_file)
                    output_file.write("\n")
        output_file.close()

    # Code Block 3: Generate output combining input and VirusTotal reports
    if code_block_3_gen_output:
        vtoutput_list = []
        with open(output_fp) as f:
            for line in f:
                vtoutput_list.append(line)

        output_file_csv = open(output_fp_csv, 'a')
        # output_file_csv.truncate(0) # Clear the file before using to avoid using stale data
        for i in range(len(url_list)):
            output_file_csv.write(input_list[i] + "," + vtoutput_list[i])

if __name__ == '__main__':
    main()

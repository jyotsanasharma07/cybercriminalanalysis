@echo off

set "projectPath=%~dp0"

echo Running 1. getTrends.py
python "%projectPath%1. getTrends.py"

echo Running 2. getLinksGoogle.py
python "%projectPath%2. getLinksGoogle.py"

echo Running 3. getLinksBing.py
python "%projectPath%3. getLinksBing.py"

echo Running 4. virusTotal.py
python "%projectPath%4. virusTotal.py"

echo All automated scripts have been executed.
pause
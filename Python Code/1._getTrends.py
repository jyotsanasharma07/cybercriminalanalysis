# *****************************************************************************************************************************
# CIS-553
# Examining the Exploitation of Trending Topics by Cybercriminals on the Internet
# Group members: A. Dalal, J.Sharma, P.Srivastav
# getTrends.py
# The following is to get the top 20 Google Trends and Twitter Trends.  The code is executed every day at same time to collect
# the collected N trends are appended to the file named TrendingTopics.csv with the dates and source
# mycredentials.py holds the keys to Google and Twitter APIs
# *****************************************************************************************************************************

from pathlib import Path
import tweepy
import pandas as pd
import mycredentials
from datetime import date
from pytrends.request import TrendReq
from tqdm import tqdm
import sys

# Get the path to the folder containing the bundled executable
if getattr(sys, 'frozen', False):
    CURRENT_DIR = Path(sys._MEIPASS)
else:
    CURRENT_DIR = Path(__file__).resolve().parent

def main():
    # Number of trending topics to request from Google
    num_trends_to_request = mycredentials.trend_per_day

    if num_trends_to_request < mycredentials.max_trend_per_day:
        print("Requesting ", num_trends_to_request, " trending topics of the day from Google...\n")

        # Initialize TrendReq object from pytrends library
        pytrends = TrendReq(hl='en-US', tz=360)
        
        # Get the top trending searches from Google for the United States
        google_trends = pytrends.trending_searches(pn='united_states')[:num_trends_to_request]

        # List to store the extracted data
        extracted_data = []

        # Append top trending Google terms to the data list
        for i in google_trends.index:
            extracted_data.append(['Google', date.today().strftime("%m/%d/%Y"), google_trends.iloc[i][0]])
            print(i+1, ' - ', google_trends.iloc[i][0])

        # Create a pandas DataFrame from the extracted data
        df = pd.DataFrame(extracted_data, columns=['Source', 'Date', 'Trending Term'])
        
        # Append the DataFrame to a CSV file
        df.to_csv(CURRENT_DIR/'_Output/TrendingTopics.csv', mode='a', index=False, header=False)
    else:
        print("Incorrect Setting for 'trend_per_day'. Please read user manual before executing the program")
        


if __name__ == '__main__':
    main()


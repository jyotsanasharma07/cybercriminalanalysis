# *****************************************************************************************************************************
# CIS-553
# Examining the Exploitation of Trending Topics by Cybercriminals on the Internet
# Group members: A. Dalal, J.Sharma, P.Srivastav
# getLinksGoogle.py
# The following is to do a Google search of all the top N trends collected and stored in TrendingTopic.csv everyday.
# There are N links collected per trending topic and all the collected URLs are stored together with data, source and trending topic
# in the file named, Links.csv.
# *****************************************************************************************************************************

from pathlib import Path
import pandas as pd
from googlesearch import search
from tqdm import tqdm
import mycredentials
import requests

def main(**kwargs):
    # Number of URLs to request for each trending topic
    NUM_LINKS_TO_REQUEST = mycredentials.url_link_per_trend_per_day

    if NUM_LINKS_TO_REQUEST < mycredentials.max_url_link_per_trend_per_day:
        # Get the parent directory of the current file
        BASE_DIR = Path(__file__).resolve().parent
        
        print("Requesting ", NUM_LINKS_TO_REQUEST, " URL for each trending topic of the day from Google...")
        
        # Read trending topics from the CSV file
        df = pd.read_csv(BASE_DIR/'_Output/TrendingTopics.csv', header=0)
        data = []

        # Iterate through the last 'trend_per_day' rows of the DataFrame
        k  = 0
        for i in df.index[len(df.index) - mycredentials.trend_per_day:]:
            # Counter for display purposes
            k = k + 1
            print('\n')

            # Get the trending term for the current row
            trend = df.loc[i, 'Trending Term'].replace('#', '')
            
            # Perform a Google search for the trending term
            query = search(trend, lang='en', num_results=NUM_LINKS_TO_REQUEST)
            
            # Iterate through the search results and append data to the list
            for j in range(NUM_LINKS_TO_REQUEST):
                url = next(query)
                data.append([df.loc[i, 'Source'], 'Google', df.loc[i, 'Date'], trend, url])
                print(k, '.', j+1, ' - ', trend, ' - ', url)
                
        # Create a DataFrame from the extracted data
        dfLinks = pd.DataFrame(data, columns=['Trending Source', 'Search Engine', 'Date', 'Trending Term', 'URL'])

        # Store the URLs together with the date, source, and trending topic in the 'Links.csv' file
        dfLinks.to_csv(BASE_DIR/'_Output/Links.csv', index=False, header=False, mode='a')
    else:
        print("Incorrect Setting for 'url_link_per_trend_per_day'. Please read user manual before executing the program")


if __name__ == '__main__':
    main()


# *****************************************************************************************************************************
# CIS-553
# Examining the Exploitation of Trending Topics by Cybercriminals on the Internet
# Group members: A. Dalal, J.Sharma, P.Srivastav
# analysis.py
# Analyze the virustotal data programtically as the data size is huge.
# *****************************************************************************************************************************

import pandas as pd
import json
from json.decoder import JSONDecodeError
import requests
from html.parser import HTMLParser
from tqdm import tqdm
from pathlib import Path
import mycredentials

BASE_DIR = Path(__file__).resolve().parent

def get_positives(scan_results):
    try:
        json_data = json.loads(scan_results)
        positives = int(json_data.get("positives", 0))
        return positives
    except (JSONDecodeError, KeyError):
        return 0

def strip_string(term):
    term = term.replace(' vs ', 'v')
    term = term.translate(str.maketrans('', '', '''!()-[]{};:'"\,<>./?@#$%^&*_~ ''')).lower()
    return term

class MyHTMLParser(HTMLParser):
    def __init__(self, *, convert_charrefs: bool = ..., term) -> None:
        super().__init__(convert_charrefs=convert_charrefs)
        self.term = term
        self.in_page_content = False
        self.in_meta_refresh = False
        self.in_outgoing_links = False

    def handle_starttag(self, tag, attrs):
        if ('meta' in str(tag)) and (('http-equiv', 'refresh') in attrs):
            self.in_meta_refresh = True
        elif str(tag) == 'a':
            if strip_string(self.term) in strip_string((str(attrs))):
                self.in_outgoing_links = True

    def handle_data(self, data):
        if strip_string(self.term) in strip_string(data):
            self.in_page_content = True

file = open(BASE_DIR/'_Output/vt_output.csv')
data = []

# Loop through each line in the file
for line in file:
    start = line.find(',{"scan_id": ')
    ls = line[:start].split(',')
    virus_total = line[start+1:]

    # Adjust the data structure based on the condition
    if ls[4] == ' N.C':
        data.append([ls[0], ls[1], ls[2], ls[3] + ',' + ls[4], ','.join(ls[5:]), virus_total])
    else:
        data.append([ls[0], ls[1], ls[2], ls[3], ls[4], virus_total])

# Create a DataFrame from the collected data
df = pd.DataFrame(data, columns=['Trending Source', 'Search Engine', 'Date', 'Trending Term', 'URL', 'VirusTotal'])

# Clean up the URL column
df.loc[:, 'URL'] = df['URL'].apply(lambda x: x.replace('"', ''))

# Add a 'Positives' column based on the VirusTotal results
df.loc[:, 'Positives'] = df['VirusTotal'].apply(get_positives)

# Output the DataFrame to an Excel file
df.to_excel(BASE_DIR/'_Output/vt_output.xlsx', index=False)

# Initialize variables for page ranking
page_rank = []
rank = 1
page_rank.append(rank)

# Calculate page rank based on trending terms
for index in df.index[1:]:
    prev_term = df.loc[index-1, 'Trending Term']
    new_term = df.loc[index, 'Trending Term']
    if new_term == prev_term:
        rank += 1
    else:
        rank = 1
    page_rank.append(rank)

# Add a 'Page Rank' column to the DataFrame
df.loc[:, 'Page Rank'] = page_rank

# Filter rows with positive results
df_positives = df[df['Positives'] > 0]

# Define columns for malware analysis
analysis_cols = ['inURL', 'inMetaRefresh', 'inPageContent', 'inOutgoingLinks']
data = []

# Reset the index of the DataFrame
df_positives = df_positives.reset_index()

# Collect malware analysis data
for i in tqdm(df_positives.index):
    url = df_positives.loc[i, 'URL']
    trending_term = df_positives.loc[i, 'Trending Term']

    # Check if the trending term is in the URL
    if strip_string(trending_term) in strip_string(url):
        in_url = True
    else:
        in_url = False

    try:
        # Make a request to the URL with a timeout of 10 seconds
        r = requests.get(url, timeout=10)
    except Exception as exception:
        # Handle exceptions and append placeholder values
        data.append([type(exception).__name__]*4)
        continue

    # Parse HTML content
    parser = MyHTMLParser(term=trending_term)
    parser.feed(r.text)

    # Append analysis data to the list
    data.append([in_url, parser.in_meta_refresh, parser.in_page_content, parser.in_outgoing_links])

# Create a DataFrame for malware analysis
df_temp = pd.DataFrame(data, columns=analysis_cols)

# Add malware analysis columns to the main DataFrame
for col in df_temp:
    df_positives.loc[:, col] = df_temp[col]

# Drop the 'index' column and output the results to an Excel file
df_positives = df_positives.drop(columns=['index'])
df_positives.to_excel(BASE_DIR/'_Output/MalwareAnalysis.xlsx', index=False)
print ("Done. Please check the output inside MalwareAnalysis.xlsx")
# *****************************************************************************************************************************
# CIS-553
# Examining the Exploitation of Trending Topics by Cybercriminals on the Internet
# Group members: A. Dalal, J.Sharma, P.Srivastav
# toxicityAnalysis.py
# Create the plot for all the expected outputs
# *****************************************************************************************************************************

import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path
import mycredentials

BASE_DIR = Path(__file__).resolve().parent

# Read the main DataFrame from the vt_output.xlsx file
df = pd.read_excel(BASE_DIR/'_Output/vt_output.xlsx')

# Convert the 'Date' column to datetime format
df['Date'] = pd.to_datetime(df['Date'], format='%m/%d/%Y')

# Read the malware analysis DataFrame from the MalwareAnalysis.xlsx file
df_positives = pd.read_excel(BASE_DIR/'_Output/MalwareAnalysis.xlsx', header=0)

# Convert the 'Date' column to datetime format
df_positives['Date'] = pd.to_datetime(df_positives['Date'], format='%m/%d/%Y')

# Group by date and aggregate the count of positives for the main DataFrame
df_date = df.groupby('Date').agg({'Positives': 'count'})

# Group by date and aggregate the sum of positives and mean of page rank for the malware analysis DataFrame
df_date_positives = df_positives.groupby('Date').agg({'Positives': 'sum', 'Page Rank': 'mean'})

# Rename the columns for better clarity
df_date.columns = ['Number of Links']

# Save the aggregated number of links DataFrame to NumOfLinks.xlsx
df_date.to_excel(BASE_DIR/'_Output/NumOfLinks.xlsx')

# Plot the number of links gathered by day
df_date.plot(y='Number of Links', use_index=True, title='Number of links gathered by day', xlabel='Date', ylabel='Number of links')
plt.show()
print ("Close this output plot to see the next one\n")

# Plot the sum of positive detections by day
df_date_positives.plot(y='Positives', use_index=True, title='Sum of positive detections by day', xlabel='Date', ylabel='Positive Detections')
plt.show()
print ("Close this output plot to see the next one\n")

# Plot the mean of the Page Rank of malicious URLs by day
df_date_positives.plot(y='Page Rank', use_index=True, title='Mean of the Page Rank of malicious URLs by day', xlabel='Date', ylabel='Page Rank')
plt.show()
print ("This is the last plot\n")

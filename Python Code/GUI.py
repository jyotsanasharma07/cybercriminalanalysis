# *****************************************************************************************************************************
# CIS-553
# Examining the Exploitation of Trending Topics by Cybercriminals on the Internet
# Group members: A. Dalal, J.Sharma, P.Srivastav
# GUI.py
# The Python code creates a Tkinter-based GUI with buttons corresponding to different project stages. 
# It enables users to execute associated Python scripts, displaying real-time outputs in the GUI console
# *****************************************************************************************************************************

import tkinter as tk
from tkinter import Text, Scrollbar
from subprocess import Popen, PIPE, STDOUT
import threading
from pathlib import Path
import sys

if getattr(sys, 'frozen', False):
    CURRENT_DIR = Path(sys._MEIPASS)
    # print('CURRENT_DIR = Path(sys._MEIPASS)')
else:
    CURRENT_DIR = Path(__file__).resolve().parent
    # print('CURRENT_DIR = Path(__file__).resolve().parent')

class ConsoleRedirector:
    def __init__(self, console_widget):
        # Constructor for the ConsoleRedirector class
        self.console_widget = console_widget

    def write(self, message):
        # Method to write a message to the console
        self.console_widget.insert(tk.END, message)
        self.console_widget.see(tk.END)  # Auto-scroll to the bottom

class GUI(tk.Tk):
    def __init__(self):
        # Constructor for the GUI class
        tk.Tk.__init__(self)
        self.title("Demo GUI")

        # Create a Text widget for the console
        self.console = Text(self, wrap="word", height=20, width=80)
        self.scrollbar = Scrollbar(self, command=self.console.yview)
        self.console.config(yscrollcommand=self.scrollbar.set)
        self.console.pack(side="top", fill="both", expand=True)
        self.scrollbar.pack(side="right", fill="y")

        # Create buttons for different script stages
        self.create_buttons()

    def create_buttons(self):
        # Method to create buttons for different script stages
        stages = ["1. getTrends", "2. getLinksGoogle", "3. getLinksBing", "4. virusTotal", "5. analysis", "6. toxicityAnalysis", "7. getIP"]

        for stage in stages:
            button = tk.Button(self, text=stage, command=lambda s=stage: self.run_script(s))
            button.pack(side="left")

    def run_script(self, stage):
        # Method to run the selected script in a separate thread
        self.console.delete(1.0, tk.END)  # Clear previous console output

        # Run the script in a separate thread to avoid freezing the GUI
        thread = threading.Thread(target=self.execute_script, args=(stage,))
        thread.start()

    def execute_script(self, stage):
        # Method to execute the selected script
        script_path = f"{stage}.py"
        full_script_path = CURRENT_DIR.joinpath(script_path)
        command = ["python", "-u", full_script_path]  # Added -u for unbuffered output

        with Popen(command, stdout=PIPE, stderr=STDOUT, bufsize=1, universal_newlines=True, text=True, encoding="utf-8") as process:
            redirector = ConsoleRedirector(self.console)
            for line in process.stdout:
                redirector.write(line)

if __name__ == "__main__":
    # Create an instance of the GUI class and start the main event loop
    app = GUI()
    app.mainloop()

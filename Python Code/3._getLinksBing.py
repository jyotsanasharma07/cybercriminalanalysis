# *****************************************************************************************************************************
# CIS-553
# Examining the Exploitation of Trending Topics by Cybercriminals on the Internet
# Group members: A. Dalal, J.Sharma, P.Srivastav
# getLinksBing.py
# The following is to do a Bing search of all the top 40 trends collected and stored in TrendingTopic.csv everyday.
# There are N links collected per trending topic per language.  The data is collected for languages
# en-US: American English
# The code can be modified in future to also run it for different geography. The codes are as below
# zh-CN: Chinese
# ar-SA: Arabic
# ru-RU: Russian
# The collected URLs are stored together with data, source and trending topic in the file named, Links.csv.
# *****************************************************************************************************************************

from pathlib import Path
from googlesearch import search
from tqdm import tqdm
import json
import pandas as pd
import mycredentials
import requests
from cryptography.fernet import Fernet

def main(**kwargs):
    # Number of URLs to request for each trending topic
    NUM_LINKS_TO_REQUEST = mycredentials.url_link_per_trend_per_day

    if NUM_LINKS_TO_REQUEST < mycredentials.max_url_link_per_trend_per_day:
        # Get the parent directory of the current file
        BASE_DIR = Path(__file__).resolve().parent
        
        print("Requesting ", NUM_LINKS_TO_REQUEST, " URL for each trending topic of the day from Bing...")
        
        # Read trending topics from the CSV file
        df = pd.read_csv(BASE_DIR /'_Output/TrendingTopics.csv', header=0, encoding='latin1')
        data = []

        # Bing Search API URL
        search_url = "https://api.bing.microsoft.com/v7.0/search?q=sailing&count=10&offset=40&mkt=en-US"

        # Call to Bing Search with 40 trending topics
        i = 0
        for k in df.index[len(df.index) - mycredentials.trend_per_day:]:
            # Counter for display purposes
            i = i + 1
            print('\n')

            # Get the trending term for the current row
            trend = df.loc[k, 'Trending Term'].replace('#', '')
            params = {"q": trend, "textDecorations": True, "textFormat": "HTML"}
            cipher_suite = Fernet(mycredentials.encryption_key)
            headers = {"Ocp-Apim-Subscription-Key": cipher_suite.decrypt(mycredentials.encrypted_bing_key).decode()}

            # Get URLs from each trend search
            for l in range(NUM_LINKS_TO_REQUEST):
                query_bing = requests.get(search_url, headers=headers, params=params)
                json_out = query_bing.json()
                url_results = json_out['webPages']['value'][l]['url']
                data.append([df.loc[k, 'Source'], 'Bing', df.loc[k, 'Date'], trend, url_results])
                print(i, '.', l+1, ' - ', trend, ' - ', url_results)

                # Sleep for 3 seconds before the next trend search to avoid Bing error
                # time.sleep(3)

        # Create a DataFrame from the extracted data
        dfLinks = pd.DataFrame(data, columns=['Trending Source', 'Search Engine', 'Date', 'Trending Term', 'URL'])

        # Store the URLs together with the date, source, and trending topic in the 'Links.csv' file
        dfLinks.to_csv(BASE_DIR/'_Output/Links.csv', index=False, header=False, mode='a')

        # Sorting the 'Links.csv' file
        csvData = pd.read_csv(BASE_DIR/'_Output/Links.csv')
        csvData.sort_values(csvData.columns[3], axis=0, inplace=True)
    else:
        print("Incorrect Setting for 'url_link_per_trend_per_day'. Please read user manual before executing the program")

if __name__ == '__main__':
    main()
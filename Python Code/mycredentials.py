# *****************************************************************************************************************************
# CIS-553
# Examining the Exploitation of Trending Topics by Cybercriminals on the Internet
# Group members: A. Dalal, J.Sharma, P.Srivastav
# getLinksBing.py
# This file contains all the keys for API calls.
# *****************************************************************************************************************************
from cryptography.fernet import Fernet

# Your encryption key (keep it secret)
encryption_key ='ylS4j-_LipdlEu0L5tElAI9EbWYlhh4cLkJuXPkfdH4='

encrypted_api_key             = 'gAAAAABlbicPYFSkP_moXJSqVz9yR325YiALdHVHpxDEVXvuORj7Gn0NM4Ucm_-6vpN-94zM918lNzXqGJTTW7rmZ-aTds1bcG8GUg12ZgpqDpsac14oeQQ='
encrypted_api_key_secret      = 'gAAAAABlbicPTa-TqQ-8Obdh3OzrVibi5wXi68GPfTO8o4vaV79h1kEreklkojwyUtMcT6eHriowZOTeXk6tUvn1EOhUHlDl_esSkHBZWIcfbkjUnvFYp8vcK9IUhWh9K1D_lEoHVU2DZEWPS89DBDm8nyZMIYIC5Q=='
encrypted_access_token        = 'gAAAAABlbicP1_NXNMA5d9f0wWFSZOFxif_h_wmEZ_yxKJUwLP7SxBJTbqvMjZua93tD5G3WONJcfWp_b7dQRO4UHlh1DBUL-Tz_cKyKW8L4TkOKqn0dHuZGZk_P-asBOkQ8K1u-65yA7s360Rlle8-1sBTRE9Th1g=='
encrypted_access_token_secret = 'gAAAAABlbicPL0-1tlrSJEEGIxp00hZlLPTyHOdISEAP6ciuz0ks_TznPFLZxZDSN9mB34AnQ1K4EiFD3z_0CyLo75m0HfS5h794sp3loXhQnwNbwXm7zFXMiQUSbVQaBVXiM9HFrMo5'
encrypted_vt_api_key          = 'gAAAAABlbicPos36ZuN6-v60MJ43fADJhXYa7ohxZPo0FLV0zey5P6CX0U-tqbz0qWXKG0jh572vb5zoGsJy4IYKMGWdk3Gc3QCq4q8CxUuTfa6ZjDpvWAa28jSeoHiCDeBVG7YUBq7tqRsGHtDfQE9AxJxjFZ66TaAziUVtmcNq8SbYrcptj0A='
encrypted_google_api_key      = 'gAAAAABlbicPJazsqRcB5X_SFu9D_Odj28Sy2OnswomkH2WZDr92lut-FGl6GrpKsdX1mgF7slGBOJ8oY3BxiQicujeTnheE-VkLXgM032GDx_HyNce2lzmoif-g9EFyK8027b9zm-tv'
encrypted_google_api_key      = 'gAAAAABlbicP5xx00PH2TG8Pa5KKFz0AE4_8ay7hh7mQSsqoFHvpc6QAciWKOm9-rcLXlc6sIVJfZykeGWXkQA07VN73FK3UNps3WWer8xLkgHBGqzzwPCI='
encrypted_bing_key            = 'gAAAAABlbicPKjBSvCaWy48vPtlZ9756UioHuCVdpJkDLntH0jx01QUto-0aoJWgvUQ3X57ZbHEmQLY-BW_7hdF1IdhjQJofBtUQ_yRwFMYc10WiWMF251dvS0WrosrXM4bpA3XFFkKO'
encrypted_apapi_key           = 'gAAAAABlbicPmXsa9O8BwIhOyiBdZqZU446VFaWVcvM1LbucjRdZXFdKAiqZ9sll7CiDIn1b6m7zFYp3FLvTEHmWWdHVeC5BuAd9N6pcaX9VQX-tTgG72fNJrn_RJy9a-Muf5e1407VR'

# User Configuration Section
trend_per_day = 2
url_link_per_trend_per_day = 1

# System Setting. Please do not change these without the permission of the program owner
max_trend_per_day = 20
max_url_link_per_trend_per_day = 5
# *****************************************************************************************************************************
# CIS-553
# Examining the Exploitation of Trending Topics by Cybercriminals on the Internet
# Group members: A. Dalal, J.Sharma, P.Srivastav
# getIP.py
# The following is to get the IP addresses of all the URLs marked as malicious and find the country & city using the IP address.
# The country and city for each URL is stored in file named, MalwareAnalysis_w_Country_City.csv
# *****************************************************************************************************************************

import pandas as pd
import socket
import requests
from tqdm import tqdm
from pathlib import Path
import mycredentials
from cryptography.fernet import Fernet

BASE_DIR = Path(__file__).resolve().parent

def main(url_count=None, **kwargs):
    # Read the Malware Analysis data from the CSV file
    malware_df = pd.read_csv(BASE_DIR / '_Output/MalwareAnalysis.csv', encoding='latin1')
    data = []

    # Iterate through each row in the Malware Analysis DataFrame
    for k in tqdm(range(len(malware_df.index)), desc='URL count', unit='URL'):
        malicious_url = malware_df.loc[k, 'URL'].replace('https://', '')
        malicious_url = malicious_url.split('/')[0]

        try:
            # Get the IP address for the URL using gethostbyname()
            ip_address = socket.gethostbyname(malicious_url)
        except socket.gaierror as e:
            # print(f"Error getting IP address for {malicious_url}: {e}")
            ip_address = '104.21.70.82'

        # API call to get additional information about the IP address using ipapi.com
        cipher_suite = Fernet(mycredentials.encryption_key)
        req = requests.get(f'http://api.ipapi.com/{ip_address}?access_key={cipher_suite.decrypt(mycredentials.encrypted_apapi_key).decode()}')
        response = req.json()

        # Append the data to the new DataFrame
        data.append([
            malware_df.loc[k, 'Trending Source'],
            malware_df.loc[k, 'Search Engine'],
            malware_df.loc[k, 'Date'],
            malware_df.loc[k, 'Trending Term'],
            malware_df.loc[k, 'URL'],
            malware_df.loc[k, 'Positives'],
            malware_df.loc[k, 'inURL'],
            malware_df.loc[k, 'inMetaRefresh'],
            malware_df.loc[k, 'inPageContent'],
            malware_df.loc[k, 'inOutgoingLinks'],
            ip_address,
            response.get("country_name"),
            response.get("city")
        ])

    # Create a new DataFrame with the collected data
    new_df = pd.DataFrame(data, columns=[
        'Trending Source', 'Search Engine', 'Date', 'Trending Term', 'URL', 'Positives',
        'inURL', 'inMetaRefresh', 'inPageContent', 'inOutgoingLinks', 'IP Address', 'Country', 'City'
    ])

    # Save the new DataFrame to a new CSV file
    new_df.to_csv(BASE_DIR / '_Output/MalwareAnalysis_w_Country_City.csv', index=False, header=True, mode='w')
    print ("Done. Please check the output inside MalwareAnalysis_w_Country_City.csv")
if __name__ == '__main__':
    main()


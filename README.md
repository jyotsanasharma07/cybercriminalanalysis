----------------------mycredentials.py-----------------------
This file contains all the keys for the python APIs and libraries used in this project and this must reside
in the directroy with all other python files for this project.

The following python files shall be executed in the same order as described below.

----------------------1.getTrends.py-------------------------
This program collects the top 20 Google trends and Twitter trends and save them to TrendingTopics.csv
TrendingTopics.csv is a running list that continues to grow everytime getTrends.py is executed.

----------------------2.getLinksGoogle.py---------------------
This program read in the last 40 trends from TrendingTopics.csv and collects 25 URLs per trending topic via Google search.
The 25 URLs per trending topic are saved to Links.csv.  Links.csv is a running list that continues to grow everytime getLiksGoogle.py is executed.

----------------------3.getLinksGoogle.py----------------------
This program read in the last 40 trends form TrendingTopics.csv and collects 10 URLs per trending topic per language used via Bing search.
getLinksBing.py is executed manually for languages English, Chinese, Arabic and Russian.  The collected URLs are saved to Links.csv.
Links.csv is a running list that continues to grow everytime getLinksBing.py is executed.

----------------------4. virusTotal.py----------------------
This program reads the 'Links.csv' and generates 'vt_output.csv'. Virustotal student key was used for the project that allows 20,000 url scan & report
everyday. The student key alos allows 1000 urls scan + report every min. If this code is run with a public key then please adjust the code variables
limit_per_min = 4 & vt_max_batch_cnt = 250. The code is built to handle the sleep time if a public key is used. 'vtresponsefile,txt' is the output from
scan api which is passed to the report function.

----------------------5.analysis.py---------------------------
This python script takes the vt_output.csv file as input.
It outputs a clean form of the same file (vt_output.xlsx)
along with the MalwareAnalysis.xlsx file which contains
the URLs that gathered a positive detection from VirusTotal
as well as the Page Rank of the URLs and four boolean values 
needed for toxicity analysis: inURL, inMetaRefresh, 
inPageContent, and inOutgoingLinks.

---------------------6.toxicityAnalysis.py--------------------
This python script takes as inputs the MalwareAnalysis.xlsx 
and the vt_output.xlsx excel files.

It aggregates the resulting dataframes by Date and outputs
three plots that correspond to the following:
1. Number of links gathered by day
2. Sum of positive detections by day
3. Mean of the Page Rank of malicious urls by day

It also outputs an excel file (NumOfLinks.xlsx) which 
corresponds to how many links we were able to gather per day.

---------------------7.getIP.py-------------------------------
NOTE: MalwareAnalysis.xlsx must be saved as csv file for this step
This program uses malwareanalysis.csv as input and find the IP address for all the URLs classified as malicious by VirusTotal.
The IP address is then used with python library ipapi to find the country and city of origin for urls.
The data from malwareanalysis.csv, country and city are saved to malwareanalysis_w_Country_City.csv.
